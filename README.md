# A Library for Mahjong Solitaire game

This library provides some types and functions which should ease Mahjong
Solitaire game development.

Current version is in its early state and might be not as efficient as possible.

The purpose of this library is to provide necessary primitives and
functionality.
