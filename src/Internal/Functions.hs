module Internal.Functions where

-- FIXME Rename Internal to General

import qualified Data.HashMap.Strict as M
import           Data.Maybe          (isNothing)
import           Internal.Type


-- * Major Use Functionality

-- | Check if tile is __free__, i.e it is not covered by other tiles and have no
-- neighbours on left and right.
isFreeTile :: TileId -> BoardMap -> Bool
isFreeTile ti (BoardMap idm posm) = maybe False isFree tile
  where tile = M.lookup ti idm
        isFree p = notCovered p && (noLeftNs p || noRightNs p)
        notCovered p = all isNothing (fmap (`M.lookup` posm) (topNs p))
        noLeftNs = noSideNs leftNs
        noRightNs = noSideNs rightNs
        noSideNs f p = all isNothing (fmap (`M.lookup` posm) (f p))

-- | Return a list of positions which can cover given position.  Tile can be
-- fully and partially covered by other tiles.
topNs :: Pos -> [Pos]
topNs (Pos x y z) =
  [ Pos (stepLeft x)  (stepUp y)   (goTop z)
  , Pos x             (stepUp y)   (goTop z)
  , Pos (stepRight x) (stepUp y)   (goTop z)
  , Pos (stepLeft x)  y            (goTop z)
  , Pos x             y            (goTop z)
  , Pos (stepRight x) y            (goTop z)
  , Pos (stepLeft x)  (stepDown y) (goTop z)
  , Pos x             (stepDown y) (goTop z)
  , Pos (stepRight x) (stepDown y) (goTop z)]

-- | Return a list of bottom neighbours.
botNs :: Pos -> [Pos]
botNs = map (\(Pos x y (LN z)) -> Pos x y (LN (z - 2))) . topNs

-- | Return a list of left neighbour positions for given one.
leftNs :: Pos -> [Pos]
leftNs (Pos x y z) =
  [ Pos (goLeft x)  (stepUp y)   z
  , Pos (goLeft x)  y            z
  , Pos (goLeft x)  (stepDown y) z]

-- | Return a list of right neighbour positions for given one.
rightNs :: Pos -> [Pos]
rightNs (Pos x y z) =
  [ Pos (goRight x)  (stepUp y)   z
  , Pos (goRight x)  y            z
  , Pos (goRight x)  (stepDown y) z]


-- | Convert a list of tiles to 'BoardMap'.
mkBoardMap :: [Tile] -> BoardMap
mkBoardMap ts = uncurry BoardMap folded
  where folded = foldBoard ts
        foldBoard = foldl insertVals (idMap, posMap)
        insertVals (im, pm) t = (insertId t im, insertPos t pm)
        insertId (Tile ti _ p) = M.insert ti p
        insertPos (Tile _ tt p) = M.insert p tt
        idMap = M.empty
        posMap = M.empty


-- * Navigation Functionality

goLeft, goRight, goUp, goDown :: LPos -> LPos
goLeft (FP x) = FP (x - 1)
goLeft (HP x) = HP (x - 1)
goRight (FP x) = FP (x + 1)
goRight (HP x) = HP (x + 1)
goUp (FP x) = FP (x - 1)
goUp (HP x) = HP (x - 1)
goDown (FP x) = FP (x + 1)
goDown (HP x) = HP (x + 1)

stepLeft, stepRight, stepUp, stepDown :: LPos -> LPos
stepLeft (FP x) = HP (x - 1)
stepLeft (HP x) = FP x
stepRight (FP x) = HP x
stepRight (HP x) = FP (x + 1)
stepUp (FP x) = HP (x - 1)
stepUp (HP x) = FP x
stepDown (FP x) = HP x
stepDown (HP x) = FP (x + 1)

goTop, goBottom :: LNum -> LNum
goTop (LN x) = LN (x + 1)
goBottom (LN x) = LN (x - 1)


-- | Check if two tiles are paired, that is could be removed from board.
pairing :: TileType -> TileType -> Bool
pairing (Bamboo    a) (Bamboo    b) = pairingByNominal a b
pairing (Character a) (Character b) = pairingByNominal a b
pairing (Circle    a) (Circle    b) = pairingByNominal a b
pairing (Wind      a) (Wind      b) = pairingByNominal a b
pairing (Dragon    a) (Dragon    b) = pairingByNominal a b
pairing (Flower _) (Flower _) = True
pairing (Season _) (Season _) = True
pairing _ _ = False

-- | Numbered tiles pairing predicate.
pairingByNominal :: Eq a => a -> a -> Bool
pairingByNominal = (==)
