{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RankNTypes    #-}
module Internal.Type where

import           Data.Hashable       (Hashable (..))
import qualified Data.HashMap.Strict as M
import           Data.Monoid         ((<>))
import           GHC.Generics        (Generic)
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------

-- * General Definitions

-- ** Board

-- | A convinient board representaion.
--
-- It holds two maps, first of them maps tiles to board positions, and second
-- maps positions to tile types.
data BoardMap = BoardMap (M.HashMap TileId Pos) (M.HashMap Pos TileType)
  deriving Show

-- | Layout representaion.  This is simple list of tiles.
data BoardState = BoardState [Tile]
  deriving Show

-- | Board layer representaion.  It is parametrized over layer's level and tiles
-- list.
data Layer = Layer LNum [Tile]

-- | Position representaion, it is parametrized over @X@ layer position, @Y@
-- layer position, and @Z@ position, that is layout level.
--
-- /Note/: Mahjong solitaire have complex grid, i.e. tiles can be arranged
-- corner by corner and corner to center, that's why 'LPos' type was defined.
--
-- @
-- +---+---+   +---+        +---+
-- |   |   |   |   |        |   |
-- |   |   |   |   +---+    |   |
-- |   |   |   |   |   |    |   |
-- +---+---+   +---+   |  +-+-+-+-+
-- |   |   |   |   |   |  |   |   |
-- |   |   |   |   +---+  |   |   |
-- |   |   |   |   |      |   |   |
-- +---+---+   +---+      +---+---+
-- @
data Pos = Pos LPos LPos LNum deriving (Eq, Generic)

-- | Layer position
--
-- It's a bit complicated naming, however the rule is simple.  Consider
-- following layout:
--
-- @
-- +---+---+
-- |   |   |
-- | 1 | 2 +---+
-- |   |   |   |
-- +-+-+-+-+ 3 |
--   |   | |   |
--   | 4 | +---+
--   |   |
--   +---+-
-- @
--
-- 'FP' stands for @full position@, and 'HP' stands for @half position@.  In the
-- example above tiles @1@ and @2@ have full positions both for @X@ and @Y@:
--
--     * Tile @1@ have @LPos (FP 1) (FP 1)@
--     * Tile @2@ have @LPos (FP 2) (FP 1)@
--
-- Tiles @3@ and @4@ are shifted by half of grid, former in @Y@ position and
-- latter in @X@ position.
--
--     * Tile @3@ have @LPos (FP 3) (HP 1)@
--     * Tile @4@ have @LPos (HP 1) (FP 2)@
--
-- So, 'HP' means @1½@
data LPos
  = FP Int
  | HP Int
  deriving (Eq, Generic)

-- | Level (or layer) number.
newtype LNum = LN Int deriving (Eq, Generic)


-- ** Tiles

-- | A board tile parametrized over identity, tile type, and board position.
data Tile = Tile TileId TileType Pos
-- Show instance defined below.
-- | Tile identity.  Each board tile __must__ have unique identity.
newtype TileId = TI Int deriving (Eq, Generic)

-- | Mahjong tile types
data TileType
  = Bamboo SimpleNum
  | Character SimpleNum
  | Circle SimpleNum
  | Wind WindName
  | Dragon DragonColor
  | Flower FlowerName
  | Season SeasonName
  deriving (Eq, Ord)

instance Enum TileType where
  fromEnum (Bamboo    n) =  0 + fromEnum n
  fromEnum (Character n) =  9 + fromEnum n
  fromEnum (Circle    n) = 18 + fromEnum n
  fromEnum (Wind      w) = 27 + fromEnum w
  fromEnum (Dragon    d) = 31 + fromEnum d
  fromEnum (Flower    f) = 34 + fromEnum f
  fromEnum (Season    s) = 38 + fromEnum s
  toEnum n
    | n < 0 = error "Out of bounds"
    | n < 9 = Bamboo $ toEnum n
    | n < 18 = Character $ toEnum m
    | n < 27 = Circle $ toEnum m
    | n < 31 = Wind $ toEnum (n - 27)
    | n < 34 = Dragon $ toEnum (n - 31)
    | n < 38 = Flower $ toEnum (n - 34)
    | n < 42 = Season $ toEnum (n - 38)
    | otherwise = error "Out of bounds"
    where m = n `mod` 9

instance Bounded TileType where
  minBound = Bamboo SN1
  maxBound = Season Winter

-- | Type safe integer representaion for enumerated tiles.
data SimpleNum = SN1 | SN2 | SN3 | SN4 | SN5 | SN6 | SN7 | SN8 | SN9
  deriving (Eq, Enum, Ord, Bounded)

-- | Winds.
data WindName
  = East
  | South
  | West
  | North
  deriving (Eq, Ord, Enum, Bounded)

-- | Dragons.
data DragonColor
  = Red
  | Green
  | White
  deriving (Eq, Ord, Enum, Bounded)

-- | Flowers.
data FlowerName
  = Plum
  | Orchid
  | Chrysanthemum
  | BambooFlower
  deriving (Eq, Ord, Enum, Bounded)

-- | Seasons.
data SeasonName
  = Spring
  | Summer
  | Authumn
  | Winter
  deriving (Eq, Ord, Enum, Bounded)
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
instance Hashable Pos

instance Hashable LPos

instance Hashable LNum

instance Hashable TileId


instance Show Tile where
  show (Tile ti tt p) =
    "[" <> show ti <> " " <> show tt <> " " <> show p <> "]"

instance Show Pos where
  show (Pos x y z) =
    "(" <> show x <> ", " <> show y <> ", " <> show z <> ")"

instance Show TileId where
  show (TI x) = "#" <> show x

instance Show LNum where
  show (LN x) = show x

instance Show LPos where
  show (FP x) = "f" <> show x
  show (HP x) = "h" <> show x

instance Show TileType where
  show (Wind w)      = "W" <> show w
  show (Dragon d)    = "D" <> show d
  show (Flower f)    = "F" <> show f
  show (Season s)    = "S" <> show s
  show (Bamboo n)    = "B" <> show n
  show (Character n) = "C" <> show n
  show (Circle n)    = "O" <> show n

instance Show SimpleNum where
  show = show . (+ 1) . fromEnum

instance Show WindName where
  show East  = "e"
  show South = "s"
  show West  = "w"
  show North = "n"

instance Show DragonColor where
  show Red   = "r"
  show Green = "g"
  show White = "w"

instance Show FlowerName where
  show Plum          = "p"
  show Orchid        = "o"
  show Chrysanthemum = "c"
  show BambooFlower  = "b"

instance Show SeasonName where
  show Spring  = "s"
  show Summer  = "S"
  show Authumn = "a"
  show Winter  = "w"
--------------------------------------------------------------------------------
