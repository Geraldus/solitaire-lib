module Solitaire.Mahjong
  ( module Solitaire.Mahjong.Tiles
  , module Solitaire.Mahjong.Board ) where

import           Solitaire.Mahjong.Board
import           Solitaire.Mahjong.Tiles
