module Solitaire.Mahjong.Tiles
  ( fullTileSet ) where

import           Internal.Type


-- | Default 'Turtle' tile set
fullTileSet :: [TileType]
fullTileSet =
  repl4 bamboos
  ++ repl4 chars
  ++ repl4 circles
  ++ repl4 winds
  ++ repl4 dragons
  ++ flowers
  ++ seasons
  where
    repl4   = concat . replicate 4
    bamboos = [Bamboo SN1    .. Bamboo SN9         ]
    chars   = [Character SN1 .. Character SN9      ]
    circles = [Circle SN1    .. Circle SN9         ]
    winds   = [Wind East     .. Wind North         ]
    dragons = [Dragon Red    .. Dragon White       ]
    flowers = [Flower Plum   .. Flower BambooFlower]
    seasons = [Season Spring .. Season Winter      ]
