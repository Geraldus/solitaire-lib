module Solitaire.Mahjong.Board
  ( BoardLayout (..)
  , positions
  , genLayoutBuild
  , defaultLayoutTileSet
  , randomState
  , isFreePos ) where

import           Internal.Functions
import           Internal.Type
import           Solitaire.Mahjong.Tiles

import qualified Data.HashSet            as HS
import           System.Random

import           Data.List               (partition, sortBy)
import           Data.Ord                (comparing)


data BoardLayout
  = Turtle
  | Test1


-- | List of all available tile positions for given layout.
positions :: BoardLayout -> [Pos]
positions Turtle =
  lBot ++ l2 ++ l3 ++ l4 ++ lTop
  -- Bottom layer
  where
    lBot =
      bRow12 1
      ++ bRow8 2
      ++ bRow10 3
      ++ bRow12 4
      ++ [Pos (FP 1)  (HP 4) (LN 1)]
      ++ [Pos (FP 14) (HP 4) (LN 1)
         ,Pos (FP 15) (HP 4) (LN 1)]
      ++ bRow12 5
      ++ bRow10 6
      ++ bRow8 7
      ++ bRow12 8
    l2 = concatMap (cRow 6 2) [2..7]
    l3 = concatMap (cRow 4 3) [3..6]
    l4 = concatMap (cRow 2 4) [4..5]
    lTop = [Pos (HP 7) (HP 4) (LN 5)]
    --
    bRow12 = cRow 12 1
    bRow8  = cRow  8 1
    bRow10 = cRow 10 1
    cRow n l y =
      let cx = 8 {- middle X of layout -}
          half = n `div` 2
          x1 = cx - half
          x2 = cx + half - 1
      in [Pos (FP x) (FP y) (LN l) | x <- [x1..x2]]
positions Test1 =
  [ Pos (FP 1) (FP 2) (LN 1)
  , Pos (FP 2) (FP 2) (LN 1)
  , Pos (FP 3) (FP 2) (LN 1)
  , Pos (FP 1) (FP 3) (LN 1)
  , Pos (FP 2) (FP 3) (LN 1)
  , Pos (FP 3) (FP 3) (LN 1)
  , Pos (HP 1) (FP 1) (LN 1)
  , Pos (HP 2) (FP 1) (LN 1)
  , Pos (HP 1) (FP 4) (LN 1)
  , Pos (HP 2) (FP 4) (LN 1)
  , Pos (HP 1) (HP 2) (LN 2)
  , Pos (HP 2) (HP 2) (LN 2) ]


-- | Return default tiles set for given layout variant
defaultLayoutTileSet :: BoardLayout -> [TileType]
defaultLayoutTileSet Turtle = fullTileSet
defaultLayoutTileSet Test1 =
  [ Bamboo SN1, Bamboo SN1
  , Bamboo SN2, Bamboo SN2
  , Circle SN1, Circle SN1
  , Character SN2, Character SN2
  , Flower Plum, Flower Plum
  , Flower Plum, Flower Plum ]

-- | Generate a random way to fill board with tiles.
--
-- Returns a list of position pairs for a given board layout.  Resulting list
-- can be used to fill board, e.g. to put pairs of interchangeable tiles in
-- given order will give a game board solvable at least in one way.
genLayoutBuild :: StdGen -> BoardLayout -> [(Pos, Pos)]
genLayoutBuild seed l = go' seed parts
  where
    parts = freePartition boardPs
    go' g (free, closed) =
      case free of
        [] -> []
        [_] -> error "genLayoutBuild: single free tile!"
        _ -> newPair : go' nextGen (free', closed')
      where
        newPair = (freeA, freeB)
        [freeA, freeB] = take 2 freeRand
        restFree = drop 2 freeRand
        (released, _) = findReleased newPair (restFree ++ closed)
        free' = HS.toList . HS.fromList $ restFree ++ released
        closed' = HS.toList $
          HS.difference (HS.fromList closed) (HS.fromList released)
        freeRand = shuffle g free
        nextGen = snd (next g)
    boardPs = positions l
    -- go (g, acc) ps =
    --   case free of
    --     [] -> acc
    --     [_] -> error "genLayoutBuild: single free tile."
    --     _ -> go (nextGen, freePair:acc) rest
    --   where
    --     (free, unfree) = partition (`isFreePos` ps) ps
    --     freePair = (freeA, freeB)
    --     [freeA, freeB] = take 2 freeRand
    --     freeRand = shuffle g free
    --     freeRest = drop 2 freeRand
    --     rest = freeRest ++ unfree
    --     nextGen = snd $ next g

-- | Generate ramdom 'BoardState' for given 'BoardLayout'.
--
-- This function is not total and supposed that
randomState :: StdGen -> BoardLayout -> BoardState
randomState g l =
  if not evenTilesNumber || not tilesAndPosQuantityMatches
     then error "Solitaire.Mahjong.randomState: invalid layout!"
     else BoardState . zipState . zipIds $ go randTiles randPositions
  where
    tiles = defaultLayoutTileSet l
    evenTilesNumber =
      case tiles of
        [] -> error "Solitaire.Mahjong.randomState: Empty tile set!"
        _ | length tiles `mod` 2 == 1 ->
            error "Solitaire.Mahjong.randomState: Odd tiles number!"
          | otherwise -> True
    tilesAndPosQuantityMatches =
      case (tiles, randPositions) of
        ([], _) -> error "Impossible.  Already covered by evenTilesNumber"
        (_, []) -> error "Solitaire.Mahjong.randomState: Empty positions list!"
        _       ->
          (length tiles == length randPositions * 2)
          || (error $
              "Soliraire.Mahjong.randomState: wrong lengthes "
              ++ show (length tiles)
              ++ " tiles and "
              ++ show (length randPositions)
              ++ " position pairs.")
    randPositions = genLayoutBuild g l
    randTiles = shuffle g tiles
    --
    zipIds = zip (map TI [1..])
    zipState = map (\(tid, (typ, pos)) -> Tile tid typ pos)
    --
    go :: [TileType] -> [(Pos, Pos)] -> [(TileType, Pos)]
    go [] _  = []
    go _  [] = []
    go ts ps =
      let (tilesPair, restTiles) = takeTiles ts
          ((posA, posB), restPos) = takePos ps
          step = zip tilesPair [posA, posB]
      in step ++ go restTiles restPos
    takeTiles [] = error "Impossible.  Already covered in go patterns."
    takeTiles (x:xs) =
      let (unmatched, x':rest) = break (pairing x) xs
          xs' = unmatched ++ rest
      in ([x, x'],xs')
    takePos [] = error "Impossible.  Already covered in go patterns."
    takePos (x:xs) = (x, xs)


-- | Check if given position is free.
isFreePos :: Pos -- ^ position to be checked
          -> [Pos] -- ^ list of all positions
          -> Bool
isFreePos t ps = uncovered && oneSideIsFree
  where
    uncovered = free nst
    oneSideIsFree = free nsl || free nsr
    free = not . find'
    nsl = leftNs  t -- neighbour positions on left
    nsr = rightNs t -- neighbour positions on right
    nst = topNs   t -- neighbour positions on top
    phs = HS.fromList ps
    find' = any (`HS.member` phs)

-- | Find new tiles that will become free when given pair will be removed.  It
-- is assumed that given pair is __not__ icluded in position list.  So, this
-- function simply checks all neightbours of given pair.
findReleased :: (Pos, Pos) -> [Pos] -> ([Pos], [Pos])
findReleased (pa, pb) ps = (free, rest)
  where
    ns = realNeighbours pa ++ realNeighbours pb
    fns = map (\n -> (isFreePos n ps, n)) ns
    free = map snd $ filter fst fns
    rest = HS.toList $ HS.difference (HS.fromList ps) (HS.fromList free)
    -- | Return a list of real nighbour positions of @p@ found in @ps@
    realNeighbours p = HS.toList (HS.intersection ns' ps')
      where
        ns' = HS.fromList . posNeighbours $ p
        ps' = HS.fromList ps
    -- | Return a list of all possible neighbour positions for given one.
    posNeighbours p = leftNs p ++ rightNs p ++ botNs p


-- | Shuffle a list using given random generator.
shuffle :: StdGen -> [a] -> [a]
shuffle seed = map snd . sortBy (comparing fst) . zip (randoms seed::[Double])

-- | Split list of positions to free positions and closed positions lists.
freePartition :: [Pos] -> ([Pos], [Pos])
freePartition ps = (map fst free, map fst unfree)
  where
    freeMap = map (\p -> (p, p `isFreePos` ps)) ps
    (free, unfree) = partition snd freeMap
